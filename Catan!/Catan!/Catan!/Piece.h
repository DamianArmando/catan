#pragma once
#include<iostream>

class Piece
{
public: 
	enum class  Resourses : uint8_t
	{
		Wood,
		Brick,
		Sheep,
		Rock,
		Hay,
		Port
	};
	enum class ConstractionCost {
		Road=2,
		Village=4,
		City=5,
		Development=3
     };

public:
	bool thief(int positionThief, int dice);

	Piece();
	
	~Piece();
};

