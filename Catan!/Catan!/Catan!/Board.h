#pragma once
#include<iostream>
//#include<graphics.h>
class Board
{
public:

	enum class HexagonalResources : uint8_t
	{
		Wood,
		Brick,
		Sheep,
		Rock,
		Hay,
		Desert,
		Port
	};
	enum class Form : uint8_t
	{

	};
	void CreateBoard();//it will create the board,with all hexagons
	void DrawHexagon(float centerX, float centerY, float radius);// will create one hexagon

	Board();
	~Board();
};

